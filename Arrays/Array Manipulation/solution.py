#!/bin/python3

import math
import os
import random
import re
import sys


# Complete the arrayManipulation function below.
def arrayManipulation(n, queries):
    summedList = [0 for x in range(n)]  # This runs in constant time
    # The idea here is >
    # every query starts adding at some point a
    # and stop adding at some point b + 1 (as it adds on b also).

    # Each query overlaps with others
    # This overlapping can be refered as the variation
    # that each query generates on each slot.
    # So, when I start adding in point A, I can add K to that slot
    # And sustract it when I get to point B + 1.

    for a, b, c in queries:  # This runs in M time (M = len(queries))
        summedList[a - 1] += c
        if b < n:
            summedList[b] -= c

    # After having stored the variations that happens in each slot
    # We start summing from 0 to n and store that value in each slot

    currentSum = 0
    maxSum = 0

    for index, i in enumerate(summedList):  # This runs in N time
        currentSum = currentSum + i
        maxSum = max(currentSum, maxSum)

    return maxSum

    # Resulting BigO = c + M + N, which is O(N + M)


if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nm = input().split()

    n = int(nm[0])

    m = int(nm[1])

    queries = []

    for _ in range(m):
        queries.append(list(map(int, input().rstrip().split())))

    result = arrayManipulation(n, queries)

    # fptr.write(str(result) + '\n')

    # fptr.close()
#!/bin/python3

import math
import os
import random
import re
import sys


# Complete the appendAndDelete function below.
def appendAndDelete(s, t, k):
    divergence_point = 0

    max_string_length = max(len(s), len(t))
    min_string_length = min(len(s), len(t))
    divergence_point = max_string_length

    for i in range(max_string_length):
        if i >= min_string_length or not s[i] == t[i]:
            divergence_point = i
            break
    L = len(s) + len(t)
    succeed = (L - 2 * divergence_point <= k)
    return 'Yes' if succeed else 'No'


if __name__ == '__main__':
    s = input()

    t = input()

    k = int(input())

    result = appendAndDelete(s, t, k)

    print(result)
